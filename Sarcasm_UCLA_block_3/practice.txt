
We will start with some practice trials to familiarize you with the task.

Use the keys [g] and [h] of the keyboard: [g] if you choose the first utterance [h] if you choose the second utterance.

Use the left and right arrow to select how confident you are in your choice, and use the space bar to confirm your selection.

Hit any key to start the practice trials