This study is about sarcasm - a form of indirect speech in which people say something that is in particular opposition to what they actually mean. For example, a person might say “Nice shoes!” to someone as a way to criticise shoes they don’t like. But this sentence, of course, can be interpreted literally where people are sincerely giving a compliment. Often, the way a person says a sentence can provide clues regarding what they really mean.

In this experiment, imagine yourself in a situation where you are talking to a friend, and then interpret the sentences as if they were speaking to you.

Hit any key to continue