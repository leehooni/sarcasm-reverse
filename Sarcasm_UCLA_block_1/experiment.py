# coding=utf-8
import time
import sys
#sys.path.append('/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages')

import gc

import os
import glob
import csv
import codecs
import datetime
import random
from psychopy import prefs
prefs.hardware['audioLib'] = ['sounddevice']

import psychtoolbox as ptb
from psychopy import sound

from psychopy import visual,event,core,gui
from fractions import Fraction

#import pyaudio

import wave
#import cleese
import scipy.io.wavfile as wav
import numpy as np

def get_stim_info(file_name, folder):
# read stimulus information stored in same folder as file_name, with a .txt extension
# returns a list of values
    stim = file_name.split("_")
    stim = stim[0]

    info_file_name = os.path.join(folder, stim+'_BPF.txt')
    info = []
    with open(info_file_name,'r') as file:
        reader = csv.reader(file,delimiter=' ')
        for row in reader:
            info.append(row)
    return info

def enblock(x, n_stims):
    # generator to cut a list of stims into blocks of n_stims
    # returns all complete blocks
    for i in range(len(x)//n_stims):
        start = n_stims*i
        end = n_stims*(i+1)
        yield x[start:end]

def generate_trial_files(subject_number=1,n_blocks=7,n_stims=100):
# generates n_block trial files per subject
# each block contains n_stim trials, randomized from folder which name is inferred from subject_number
# returns an array of n_block file names
    seed = time.getTime()
    random.seed(seed)

#    stim_folder = "sounds/pitch_str_0_smile_0__rough_1"
#    sound_files = [os.path.basename(x) for x in glob.glob(stim_folder+"/*.wav")]
#    print(sound_files)

    stim_folder = os.path.dirname(os.path.abspath( __file__ )) + "/sounds"
    sound_folders = [os.path.basename(x) for x in os.listdir(stim_folder)]
    random.shuffle(sound_folders)
    #print(sound_folders)

    n_cond = 0
    sound_files = []
    for y in sound_folders:
        #print(y)
        #print(glob.glob(stim_folder+"/"+y+"/*.wav"))
        sound_files.append([os.path.basename(x) for x in glob.glob(stim_folder+"/"+y+"/*.wav")])
        random.shuffle(sound_files[n_cond])
        n_cond += 1


    first_half = []
    second_half = []

    for ll in sound_files:
        first_half.append(ll[:len(ll)//2])
        second_half.append(ll[len(ll)//2:])

    first_half = [item for sublist in first_half for item in sublist]
    second_half = [item for sublist in second_half for item in sublist]

    ind_stim = np.arange(len(first_half))
#    print(ind_stim)
    random.shuffle(ind_stim)
#    print(ind_stim)
    first_half = [first_half[i] for i in ind_stim]
    second_half = [second_half[i] for i in ind_stim]

    # trials consist of two random files, one from the first half, and one from the second half of the stimulus list
    # write trials by blocks of n_stims
    block_count = 0
    trial_files = []
    for block_stims in enblock(list(zip(first_half, second_half)),n_stims):
        trial_file = os.path.dirname(os.path.abspath( __file__ )) + '/trials/trials_subj' + str(subject_number) + '_' + str(block_count) + '_' + date.strftime('%y%m%d_%H.%M')+'.csv'
        print("generate trial file " +trial_file)
        trial_files.append(trial_file)
        with open(trial_file, 'w+') as file :
            # each trial is stored as a row in a csv file, with format:
            # StimA,MeanA,PA1,PA2,PA3,PA4,PA5,PA6,PA7,StimB,MeanB,PB1,PB2,PB3,PB4,PB5,PB6,PB7
            # where Mean and P1...P7 are CLEESE parameters found in .txt files stored alongside de .wav stims
            # write header
            writer = csv.writer(file)
            writer.writerow(["StimA","StimB"])
            # write each trial in block
            for trial_stims in block_stims:
                writer.writerow(trial_stims)
        # break when enough blocks
        block_count += 1
        if block_count >= n_blocks:
            break

    return trial_files

def generate_practice_trial_file(subject_number=1, n_practice_trials = 3):
# generates one file of practice trials
# the block contains a fixed nb of trials, selected randomly from the real trials
# returns one file_name

    seed = time.getTime()
    random.seed(seed)
    stim_folder = os.path.dirname(os.path.abspath( __file__ )) + "/sounds"
    sound_folders = [os.path.basename(x) for x in os.listdir(stim_folder)]
    random.shuffle(sound_folders)
    #print(sound_folders)

    n_cond = 0
    sound_files = []
    for y in sound_folders:
        #print(y)
        #print(glob.glob(stim_folder+"/"+y+"/*.wav"))
        sound_files.append([os.path.basename(x) for x in glob.glob(stim_folder+"/"+y+"/*.wav")])
        random.shuffle(sound_files[n_cond])
        n_cond += 1

    first_half = []
    second_half = []

    for ll in sound_files:
        first_half.append(ll[:len(ll)//2])
        second_half.append(ll[len(ll)//2:])

    first_half = [item for sublist in first_half for item in sublist]
    second_half = [item for sublist in second_half for item in sublist]

    # each trial is stored as a row in a csv file, with format: StimA,StimB
    trial_file = os.path.dirname(os.path.abspath( __file__ )) + '/trials/trials_subj' + str(subject_number) + '_PRACTICE_' + date.strftime('%y%m%d_%H.%M')+'.csv'
    with open(trial_file, 'w+') as file :
        # write header
        writer = csv.writer(file)
        writer.writerow(["StimA","StimB"])
        # write n_practice_trials
        for trial_stims in list(zip(first_half, second_half))[:n_practice_trials]:
            writer.writerow(trial_stims)
    return trial_file

def read_trials(trial_file):
# read all trials in a block of trial, stored as a CSV trial file
    with open(trial_file, 'rt') as fid :
        reader = csv.reader(fid)
        trials = list(reader)
    return trials[1::] #trim header

#def generate_result_file(subject_number,n_blocks):
def generate_result_file(subject_number):
    
#    result_files = []
#    for block_count in range(n_blocks):
#        result_file = os.path.dirname(os.path.abspath( __file__ )) + '/results/results_subj'+ str(subject_number) + '_' + str(block_count) + '_' + date.strftime('%y%m%d_%H.%M')+'.csv'
    result_file = os.path.dirname(os.path.abspath( __file__ )) + '/results/results_subj'+ str(subject_number) + '_' + date.strftime('%y%m%d_%H.%M')+'.csv'
    result_headers = ['subj','trial','block','sex','age','date','stim','stim_order', 'segment','segment_time','pitch','time_str','smile','rough','response','rt','conf','conf_rt']
    with open(result_file, 'w+') as file:
        writer = csv.writer(file)
        writer.writerow(result_headers)
#        result_files.append(result_file)
#    return result_files
    return result_file

def show_text_and_wait(file_name = None, message = None):
    event.clearEvents()
    if message is None:
        with codecs.open (file_name, "r", "utf-8") as file :
            message = file.read()
    text_object = visual.TextStim(win, text = message, color = 'black')
    text_object.height = 0.05
    text_object.draw()
    win.flip()
    while True :
        if len(event.getKeys()) > 0:
            core.wait(0.2)
            break
        event.clearEvents()
        core.wait(0.2)
        text_object.draw()
        win.flip()

def show_text(file_name = None, message = None):
    if message is None:
        with codecs.open (file_name, "r", "utf-8") as file :
            message = file.read()
    text_object = visual.TextStim(win, text = message, color = 'black')
    text_object.height = 0.05
    text_object.draw()
    win.flip()

def update_trial_gui():
    play_instruction.draw()
    play_icon.draw()
    response_instruction.draw()
    play_icon.draw()
    for response_label in response_labels: response_label.draw()
    for response_checkbox in response_checkboxes: response_checkbox.draw()
    win.flip()

def get_false_feedback(min,max):
# returns a random percentage (int) between min and max percent
# min, max: integers between 0 and 100
    return int(100*random.uniform(float(min)/100, float(max)/100))

def play_sound(soundfile):
        #play sound
#        audio = pyaudio.PyAudio()
#        sr,wave = wav.read(fileName)
        
        wf = sound.Sound(soundfile)
#        print(wf.__dict__.keys())
        wf.play()        
        core.wait(wf.duration)

# NB: s'attend à un format int16.
        # ffmpeg input_file_int16.wav -i input_file.wav  -f s16le -acodec pcm_s16le -ar 44100
#        def play_audio_callback(in_data, frame_count, time_info,status):
#            data = wf.readframes(frame_count)
#            return (data, pyaudio.paContinue)
        #define data stream for playing audio and start it
#        output_stream = audio.open(format   = audio.get_format_from_width(wf.getsampwidth())
#                             , channels     = wf.getnchannels()
#                             , rate         = wf.getframerate()
#                             , output       = True
#                             , stream_callback = play_audio_callback
#                        )
#        output_stream.start_stream()
#        while output_stream.is_active():
#            core.wait(0.01)
#            continue
#        output_stream.stop_stream()
#        output_stream.close()
#        audio.terminate()


###########################################################################################
###      DEFINE HOW MANY TRIALS IN HOW MANY BLOCKS
###      NOTE: if repeat_for_internal_noise, the last block is automatically repeated,
### 	 i.e. the total nb of blocks is n_block + 1
###########################################################################################

n_blocks = 4
n_stims = 60
repeat_for_internal_noise = 1
n_practice_trials = 3

###########################################################################################

img_path = os.path.dirname(os.path.abspath( __file__ )) + '/images/'
sound_path = os.path.dirname(os.path.abspath( __file__ )) + '/sounds/'

# get participant nr, age, sex
subject_info = {u'Number':1, u'Age':20, u'Sex': u'f/m'}
dlg = gui.DlgFromDict(subject_info, title=u'REVCOR')
if dlg.OK:
    subject_number = subject_info[u'Number']
    subject_age = subject_info[u'Age']
    subject_sex = subject_info[u'Sex']
else:
    core.quit() #the user hit cancel so exit
date = datetime.datetime.now()
time = core.Clock()

# create stimuli if folder don't exist
# warning: if folder exists with wrong number of stims
output_folder = sound_path
#if not os.path.exists(output_folder):
#	generate_stimuli(subject_number, n_blocks=n_blocks, n_stims=n_stims, base_sound='./sounds/male_vraiment_flat.wav', config_file='./config.py')

win = visual.Window([1366,768],fullscr=False,color="lightgray", units='norm')
screen_ratio = (float(win.size[1])/float(win.size[0]))
isi = 1

# trial gui
question = u'Which version sounds more sarcastic?'
response_options = ['[g] voice 1','[h] voice 2']
response_keys = ['g', 'h']
label_size = 0.08
play_instruction = visual.TextStim(win, units='norm', text='[Space] Voice 1 & 2', color='red', height=label_size, pos=(0,0.5))
response_instruction = visual.TextStim(win, units='norm', text=question, color='black', height=label_size, pos=(0,0.1), alignHoriz='center')
play_icon = visual.ImageStim(win, image=img_path+'play_off.png', units='norm', size = (0.15*screen_ratio,0.15), pos=(0,0.5+2*label_size))
response_labels = []
response_checkboxes = []
reponse_ypos = -0.2
reponse_xpos = -0.1
label_spacing = abs(-0.8 - reponse_ypos)/(len(response_options)+1)
for index, response_option in enumerate(response_options):
    y = reponse_ypos - label_spacing * index
    response_labels.append(visual.TextStim(win, units = 'norm', text=response_option, alignHoriz='left', height=label_size, color='black', pos=(reponse_xpos,y)))
    response_checkboxes.append(visual.ImageStim(win, image=img_path+'rb_off.png', size=(label_size*screen_ratio,label_size), units='norm', pos=(reponse_xpos-label_size, y-label_size*.05)))


# generate data files
result_file = generate_result_file(subject_number)
trial_files = generate_trial_files(subject_number,n_blocks,n_stims)

# add practice block in first positio
practice_file = generate_practice_trial_file(subject_number, n_practice_trials)
trial_files.insert(0, practice_file)
# duplicate last block (for internal noise computation)
if repeat_for_internal_noise:
	trial_files.append(trial_files[-1])

# experiment
show_text_and_wait(file_name=os.path.dirname(os.path.abspath( __file__ )) + "/intro1.txt")
show_text_and_wait(file_name=os.path.dirname(os.path.abspath( __file__ )) + "/intro2.txt")
show_text_and_wait(file_name=os.path.dirname(os.path.abspath( __file__ )) + "/practice.txt")
trial_count = 0
n_blocks = len(trial_files)
#result_files = generate_result_file(subject_number,n_blocks)

for block_count, trial_file in enumerate(trial_files):
    block_trials = read_trials(trial_file)
    for trial in block_trials :

        txt = trial[0].split(".")
        condition = txt[-2]
        cond_v = condition.split("_")
        cond_v = [cond_v[2], cond_v[4], cond_v[6]]

        #print(condition,cond_v)

        # focus play instruction and reset checkboxes
        play_instruction.setColor('red')
        play_icon.setImage(img_path+'play_on.png')
        for checkbox in response_checkboxes:
            checkbox.setImage(img_path+'rb_off.png')
        sound_1 = sound_path+condition+'/'+trial[0]
        sound_2 = sound_path+condition+'/'+trial[1]
        #print(sound_1,sound_2)
        end_trial = False
        while (not end_trial):
            update_trial_gui()
            # upon play command...
            if event.waitKeys()==['space']:
                # unfocus play instruction
                play_instruction.setColor('black')
                play_icon.setImage(img_path+'play_off.png')
                update_trial_gui()
                # play sounds
                play_sound(sound_1)
                core.wait(isi)
                play_sound(sound_2)
                # focus response instruction
                response_start = time.getTime()
                response_instruction.setColor('red')
                update_trial_gui()
                # upon key response...
                response_key = event.waitKeys(keyList=response_keys)
                response_time = time.getTime() - response_start
                # unfocus response_instruction, select checkbox
                response_instruction.setColor('black')
                response_checkboxes[response_keys.index(response_key[0])].setImage(img_path+'rb_on.png')
                update_trial_gui()

                scale=visual.RatingScale(win, low=1, high=4,  markerStart=1
                                        , labels=(u"Not at all confident",u"Very confident")
                                        , textColor='black', lineColor='black'
                                        , marker='triangle', markerColor='red'
                                        , leftKeys='left', rightKeys='right'
                                        , noMouse=True, pos = (0.0, 0.0)
                                        , precision = 1, size = 1, stretch = 1.2, textSize = 1.0, scale=(u"How confident are you with your choice?")#unicode(question, "utf-8")
                                        , respKeys=(['1','2','3','4'])  # Or ['num_1','num_2','num_3','num_4','num_5','num_6','num_7','num_8','num_9']
                                        , acceptKeys=(['return','space','num_enter']), skipKeys=None
                                        , acceptPreText='Choose', acceptText='Validate', acceptSize=1
                                        , showValue=True)
                #update_trial_gui()
                while scale.noResponse:
                    scale.draw()
                    win.flip()

                conf_response = scale.getRating()
                conf_response_time = scale.getRT()



                # blank screen and end trial
                core.wait(0.3)
                win.flip()
                core.wait(0.2)
                end_trial = True
                
                gc.collect()


        # log response
        row = [subject_number, trial_count, block_count, subject_sex, subject_age, date]
        if response_key == ['g']:
            response_choice = 0
        elif response_key == ['h']:
            response_choice = 1

#        with open(result_files[block_count], 'a') as file :
        with open(result_file, 'a') as file :
            writer = csv.writer(file,lineterminator='\n')
            for stim_order,stim in enumerate(trial):
                segments = get_stim_info(folder=sound_path+'pitch_str_0_smile_0_rough_0', file_name=stim)
                previous_time = 0.
                for segment_counter,segment_info in enumerate(segments) :
                    current_time = (float(segment_info[0]) - previous_time) / 2
                    if segment_counter % 2 == 1:
                        result = row + [stim,stim_order,segment_counter//2,current_time,segment_info[1],cond_v[0],cond_v[1],cond_v[2],response_choice==stim_order,round(response_time,3),conf_response,round(conf_response_time,3)]
                        writer.writerow(result)
                        previous_time = current_time
        trial_count += 1

    # inform end of practice at the end of first block
    if block_count == 0:
    #   show_text_and_wait(file_name=os.path.dirname(__file__)+"/end_practice.txt")
       show_text_and_wait(os.path.dirname(os.path.abspath( __file__ )) + "/end_practice.txt")
       practice_block = False
    # pause at the end of subsequent blocks
    elif block_count < n_blocks-1:
#        show_text_and_wait(message = u"Vous avez fait "+str(Fraction(block_count, n_blocks-1))+u" de l'expérience.\n Votre score sur cette partie de l'expérience est de "+ str(get_false_feedback(70,85)) +u"%.\n\n Vous pouvez prendre une pause de quelques secondes, puis appuyer sur une touche pour continuer).")
        show_text_and_wait(message = u"You did "+str(Fraction(block_count, n_blocks-1))+u" of the experiment.\n \n You may have a short break, and hit any key to continue.")

#End of experiment
#show_text_and_wait(os.path.dirname(__file__)+"/end.txt")
show_text_and_wait(os.path.dirname(os.path.abspath( __file__ )) + "/end.txt")

# Close Python
win.close()
#core.quit()
#sys.exit()
