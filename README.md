# sarcasm-reverse

Python scripts for an audio-based psychological experiment on sarcasm. The original experiment was run with [PsychoPy v3.0 standalone](https://www.psychopy.org/download.html).

Participants are instructed with a definition of sarcasm. They judge stimuli presented by pairs, by choosing which one is the more sarcastic, and a separate self-confidence (4-points confidence scale) rating per pair.

Sounds files for the three blocks of the experiment (licence CC0): [https://archive.org/details/sarcasm-reverse_sounds_202003](https://archive.org/details/sarcasm-reverse_sounds_202003)

The 27 sound subfolders for each block have to be copied in the "sounds" folder of the corresponding block.

Experiment originally run at UCLA Communication Department in February 2020

Contact: Marco Liuni
leehooni@gmail.com

# Instructions to run the experiment

Open the file experiment.py with PsychoPy3 standalone. Alternatively, run the file experiment.py with python (tested for version 3.6), by first [installing Psychopy](https://www.psychopy.org/download.html).